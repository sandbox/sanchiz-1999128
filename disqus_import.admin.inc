<?php
/**
 * @file
 * Administration forms for the Disqus import module.
 */

/**
 * Menu callback; Displays the import form.
 */
function disqus_import_admin_settings($form, &$form_state) {
  global $user;
  $formats = filter_formats($user);
  $format_values = array();
  foreach ($formats as $value) {
    $format_values[$value->format] = $value->name;
  }
  $form = array();
  $form['#attributes'] = array(
    'enctype' => 'multipart/form-data',
  );
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('Import file'),
    '#description' => t('The *.xml file generated in the <a target="_blank" href="@disqustools">Disqus import tool</a>.',
      array(
        '@disqustools' => 'http://import.disqus.com/' . variable_get('disqus_domain', '') . '/')
    ),
  );
  $form['format'] = array(
    '#type' => 'select',
    '#title' => t('Comment format'),
    '#options' => $format_values,
    '#default_value' => 'plain_text',
    '#description' => t('The format in which to save the comments'),
  );
  $form['title_type'] = array(
    '#type' => 'radios',
    '#title' => t('Forming comment title'),
    '#options' => array(
      'none' => t('None'),
      'from_body' => t('From comment body'),
      'from_node' => t('From node title'),
    ),
    '#default_value' => 'none',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );
  return $form;
}

/**
 * Form validate handler for the import tool.
 */
function disqus_import_admin_settings_validate($form, &$form_state) {
  // See if the file was successfully saved.
  $filepath = 'public://disqus_import/';
  file_prepare_directory($filepath, FILE_CREATE_DIRECTORY);
  if ($file = file_save_upload('file', array('file_validate_extensions' => array('xml')), $filepath)) {
    $xml = simplexml_load_file($file->uri);
    if ($xml === FALSE || !isset($xml->thread) || !isset($xml->post)) {
      form_set_error('file', t('Invalid XML file uploaded.'));
    }
    else {
      $form_state['values']['xml'] = $xml;
    }
  }
  else {
    form_set_error('file', t('Invalid XML file uploaded.'));
  }
}

/**
 * Form submit handler for the import tool.
 */
function disqus_import_admin_settings_submit($form, &$form_state) {
  $xml = $form_state['values']['xml'];
  unset($form_state['values']['xml']);
  $format = $form_state['values']['format'];
  $title_type = $form_state['values']['title_type'];
  $threads = disqus_import_get_threads($xml);
  disqus_import_run_processing($xml, $format, $title_type, $threads);
}

/**
 * Run inmport.
 */
function disqus_import_run_processing($xml, $format, $title_type, $threads) {
  foreach ($xml->post as $post) {
    if ($comment_data = disqus_import_get_comment_data($post, $threads)) {
      $operations[] = array(
        'disqus_import_batch_process',
        array(
          $comment_data,
          $format,
          $title_type,
        ),
      );
    }
  }
  $batch = array(
    'operations' => $operations,
    'finished' => 'disqus_import_batch_finished',
    'title' => t('Import disqus comments'),
    'progress_message' => t('Completed @current of @total.'),
    'error_message' => t('An error has occurred.'),
    'file' => drupal_get_path('module', 'disqus_import') . '/disqus_import.admin.inc',
  );
  batch_set($batch);
}

/**
 * Batch process function.
 */
function disqus_import_batch_process($comment_data, $format, $title_type, &$context) {
  if (empty($context['results'])) {
    $context['results']['comment_cids'] = array();
    $context['results']['count'] = 0;
  }

  // Create comment.
  if ($cid = disqus_import_create_comment($comment_data, $format, $title_type, $context)) {
    $context['results']['comment_cids'][$comment_data['dsq_id']] = $cid;
    $context['results']['count']++;
  }
}

/**
 * Batch finish function.
 */
function disqus_import_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(format_plural($results['count'], 'Imported @count comment.
    ', 'Imported @count comments.'));
  }
  else {
    drupal_set_message(t('Completed with errors.'), 'error');
  }
}
