README.txt
==========

The module allows you to import comments from a Disqus service
to native comments Drupal.

USAGE:
==========
Set up and use:
1) Install and activate the module.
2) Go to the site http://import.disqus.com and do export
comments from Disqus into XML format.
After completing the export to email you will file with the XML file.
3) Go to the page of the site admin/config/services/disqus-import.
4) Select the XML file, the resulting export.
5) Choosing the settings formation comments Drupal.
6) Import.
